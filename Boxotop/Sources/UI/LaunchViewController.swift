//
//  LaunchViewController.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet weak var labelBoxotop: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelBoxotop.text = NSLocalizedString("boxotop_name", comment: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        UIView.animate(withDuration: 1.0, animations: {
            self.labelBoxotop.frame.origin.y = UIScreen.main.bounds.size.height/2 - self.labelBoxotop.frame.size.height/2
            self.labelBoxotop.alpha = 1.0
        }, completion:{(finished: Bool) in
            self.performSegue(withIdentifier: "show_app", sender:self)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
