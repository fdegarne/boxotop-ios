//
//  DetailsMovieViewController.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SwiftHTTP

class DetailsMovieViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageViewMovie: UIImageView!
    @IBOutlet weak var releaseLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var audienceLabel: UILabel!
    @IBOutlet weak var releaseLabelValue: UILabel!
    @IBOutlet weak var runtimeLabelValue: UILabel!
    @IBOutlet weak var languageLabelValue: UILabel!
    @IBOutlet weak var mReviewLabel: UILabel!
    @IBOutlet weak var synopsisLabel: UILabel!
    @IBOutlet weak var synopsisLabelValue: UILabel!
    @IBOutlet weak var castingLabel: UILabel!
    @IBOutlet weak var castingLabelValue: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var directorLabelValue: UILabel!
    @IBOutlet weak var writersLabel: UILabel!
    @IBOutlet weak var writersLabelValue: UILabel!
    
    @IBOutlet weak var audienceRatingView: HCSStarRatingView!
    @IBOutlet weak var mRatingView: HCSStarRatingView!
    
    var movie:Movie?
    var movieDetails:MovieDetails?
    var queryManager:QueryManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Details", comment: "")
        titleLabel.text = movie?.title
        imageViewMovie.imageFromServerURL((movie?.poster)!, placeHolder: nil)
        
        releaseLabel.text = NSLocalizedString("release_label", comment: "")
        languageLabel.text = NSLocalizedString("language_label", comment: "")
        audienceLabel.text = NSLocalizedString("audience_label", comment: "")
        mReviewLabel.text = NSLocalizedString("my_review_label", comment: "")
        synopsisLabel.text = NSLocalizedString("synopsis_label", comment: "")
        castingLabel.text = NSLocalizedString("casting_label", comment: "")
        directorLabel.text = NSLocalizedString("direction_label", comment: "")
        writersLabel.text = NSLocalizedString("writer_label", comment: "")
        runtimeLabel.text = NSLocalizedString("runtime_label", comment: "")
        
        self.queryManager = QueryManager(imdbID: (movie?.imdbID)!)
        loadMovieDetails()
    }
    
    func loadMovieDetails(){
        let url = NetworkConfig.base_url + (self.queryManager?.getDetailsUrl())!
        HTTP.GET(url){response in
            
            if let err = response.error {
                print("error: \(err.localizedDescription)")
                return //also notify app of failure as needed
            }
            do {
                let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? NSDictionary
                self.movieDetails = MovieDetails(json: json!)
                DispatchQueue.main.async { [weak self] in
                    self?.releaseLabelValue.text = self?.movieDetails?.release
                    self?.runtimeLabelValue.text = self?.movieDetails?.runtime
                    self?.languageLabelValue.text = self?.movieDetails?.language
                    self?.audienceRatingView?.value = (self?.movieDetails?.imdbRating)! / 2.0
                    self?.synopsisLabelValue.text = self?.movieDetails?.synopsis
                    self?.castingLabelValue.text = self?.movieDetails?.casting
                    self?.directorLabelValue.text = self?.movieDetails?.director
                    self?.writersLabelValue.text = self?.movieDetails?.writers
                    
                }
                //print(self.arrayOfMovies.description)
            }catch let error as NSError{
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
