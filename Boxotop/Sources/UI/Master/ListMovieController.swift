//
//  ViewController.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit
import SwiftHTTP

class ListMovieController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var viewNoResearch: UIView!
    @IBOutlet weak var labelResearch: UILabel!
    
    @IBOutlet weak var tableViewMovies: UITableView!
    @IBOutlet weak var viewActivityIndicator: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var queryManager : QueryManager?
    var arrayOfMovies : NSMutableArray!
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("boxotop_name", comment: "")
        arrayOfMovies = NSMutableArray()
        searchBar.placeholder = NSLocalizedString("search_hint", comment: "")
        labelResearch.text = NSLocalizedString("search_info", comment: "")
        if #available(iOS 10.0, *) {
            self.tableViewMovies.refreshControl = refreshControl
        } else {
            self.tableViewMovies.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    @objc func refreshData(_ sender: Any) {
        arrayOfMovies = NSMutableArray()
        self.queryManager?.page = 0
        searchMovies(searchText: (self.queryManager?.search)!)
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        arrayOfMovies = NSMutableArray()
        self.queryManager = QueryManager()
        searchBar.endEditing(true)
        self.searchMovies(searchText: searchBar.text!)
        NSLog("test search")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        arrayOfMovies = NSMutableArray()
        self.queryManager = QueryManager()
        searchBar.endEditing(true)
        self.searchMovies(searchText: searchBar.text!)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            self.searchMovies(searchText: (self.queryManager?.search)!);
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfMovies.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "details", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as! MovieTableViewCell
        if indexPath.row%2 == 1 {
            cell.backgroundColor = UIColor(red: 170.0/255, green: 204/255.0, blue: 161/255.0, alpha: 1.0)
        }
        else{
            cell.backgroundColor = UIColor.groupTableViewBackground
        }
        
        let movie = self.arrayOfMovies.object(at: indexPath.row) as! Movie
        
        cell.imageMovie.imageFromServerURL(movie.poster!, placeHolder: nil)
        cell.labelMovie.text = movie.title
        cell.labelYear.text = movie.year
        return cell
    }

    func searchMovies(searchText:String){
        self.viewActivityIndicator.isHidden = false
        let url = NetworkConfig.base_url + (self.queryManager?.getQueryUrl(search: searchText, page: (self.queryManager?.page)! + 1))!
        HTTP.GET(url){response in
            
            if let err = response.error {
                print("error: \(err.localizedDescription)")
                return //also notify app of failure as needed
            }
            do {
                let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? NSDictionary
                let array:NSArray = json?.object(forKey: "Search") as! NSArray
                for mov in array{
                    let movie:Movie = Movie(json: mov as! NSDictionary)
                    self.arrayOfMovies.add(movie)
                }
                DispatchQueue.main.async { [weak self] in
                    self?.viewActivityIndicator.isHidden = true
                    self?.tableViewMovies.isHidden = false
                    self?.tableViewMovies.reloadData()
                    self?.refreshControl.endRefreshing()
                }
                print(self.arrayOfMovies.description)
            }catch let error as NSError{
                print(error.description)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details"{
            let index = self.tableViewMovies.indexPathForSelectedRow
            let movie = self.arrayOfMovies.object(at: (index?.row)!) as! Movie
            let dmvc:DetailsMovieViewController = segue.destination as! DetailsMovieViewController
            dmvc.movie = movie
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
