//
//  MovieTableViewCell.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var imageMovie: UIImageView!
    @IBOutlet weak var labelMovie: UILabel!
    @IBOutlet weak var labelYear: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
