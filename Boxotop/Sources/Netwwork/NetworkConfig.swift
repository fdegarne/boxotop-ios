//
//  NetworkConfig.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import Foundation

public struct NetworkConfig{
    static let scheme = "http"
    static let host = "www.omdbapi.com"
    static let apikey = "4f209d26"
    
    static let base_url = scheme + "://" + host + "/?apikey=" + apikey
    static let default_type = "movie"
}
