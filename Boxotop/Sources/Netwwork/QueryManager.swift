//
//  QueryManager.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit

class QueryManager: NSObject {
    var search:String?
    var type:String?
    var imdbID:String?
    var page:NSInteger?
    
    required override init() {
        super.init()
        self.search = ""
        self.page = 0
        self.type = NetworkConfig.default_type
    }
    
    init(search: String, page: NSInteger, type: String) {
        super.init()
        self.search = search
        self.page = page
        self.type = type
    }
    
    init(imdbID:String){
        super.init()
        self.imdbID = imdbID
    }
    
    func getQueryUrl(search:String, page:NSInteger) -> String {
        self.search = search
        self.page = page
        return String.init(format: "&s=%@&type=%@&page=%i", self.search!, self.type!, self.page!)
    }
    
    func getDetailsUrl() -> String{
        return "&i=" + self.imdbID!
    }
}
