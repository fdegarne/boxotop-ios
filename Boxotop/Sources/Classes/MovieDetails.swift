//
//  MovieDetails.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit

class MovieDetails: Movie {
    var release:String?
    var runtime:String?
    var director:String?
    var synopsis:String?
    var imdbRating:CGFloat?
    var language:String?
    var writers:String?
    var casting:String?
    
    init(title: String, year: String, imdbID: String, poster: String, release:String, runtime:String, director:String, synopsis:String, imdbRating:CGFloat, language:String, writers:String, casting:String) {
        super.init(title: title, year: year, imdbID: imdbID, poster: poster)
        self.release = release
        self.runtime = runtime
        self.director = director
        self.synopsis = synopsis
        self.imdbRating = imdbRating
        self.language = language
        self.writers = writers
        self.casting = casting
    }
    
    override init(json: NSDictionary) {
        super.init(json: json)
        let release = json.value(forKey: "Released") as? String
        let runtime = json.value(forKey: "Runtime") as? String
        let director = json.value(forKey: "Director") as? String
        let synopsis = json.value(forKey: "Plot") as? String
        let imdbRating = CGFloat(truncating: NumberFormatter().number(from: (json.value(forKey: "imdbRating") as? String)!)!)
        let language = json.value(forKey: "Language") as? String
        let writers = json.value(forKey: "Writer") as? String
        let casting = json.value(forKey: "Actors") as? String
        
        print("rating")
        print(imdbRating)
        
        self.release = release
        self.runtime = runtime
        self.director = director
        self.synopsis = synopsis
        self.imdbRating = imdbRating
        self.language = language
        self.writers = writers
        self.casting = casting
    }
}
