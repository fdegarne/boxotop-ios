//
//  Movie.swift
//  Boxotop
//
//  Created by Fabien Degarne on 20/09/2018.
//  Copyright © 2018 fabiendegarne. All rights reserved.
//

import UIKit

class Movie: NSObject {
    var title:String?
    var year:String?
    var imdbID:String?
    var poster:String?
    
    init(title:String, year:String, imdbID:String, poster:String) {
        super.init()
        self.title = title
        self.imdbID = imdbID
        self.poster = poster
        self.year = year
    }
    
    init(json: NSDictionary) {
        super.init()
        let title = json.value(forKey: "Title") as? String
        let year = json.value(forKey: "Year") as? String
        let poster = json.value(forKey: "Poster") as? String
        let imdbID = json.value(forKey: "imdbID") as? String
        
        self.title = title
        self.imdbID = imdbID
        self.poster = poster
        self.year = year
    }
}
